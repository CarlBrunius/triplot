# Welcome to the triplot package
This is an R package for visualization of correlations and risks associated with multivariate component-based models, such as PCA, factor analysis and PLS

## Installation
Install `devtools` to be able to install packages from GitHub.

Install `triplot` package from `R console` by:

`devtools::install_gitlab("CarlBrunius/triplot", build_vignettes=T)`

## Tutorial
An easy-to-follow tutorial on how to use the triplot package for visualization of correlations and risks associated with multivariate component-based models can be found at this repository under the *vignettes* folder (https://gitlab.com/CarlBrunius/triplot/blob/master/vignettes/Triplot_Tutorial.md)

## Description
The algorithm is described in detail in the paper:

*Schillemans T, Shi L, Liu X, Åkesson A, Landberg R, Brunius C, 2019. Visualization and interpretation of multivariate associations with disease risk markers and disease risk – The triplot. Metabolites.*

## Version history
version | date  | comment
:-----: | :---: | :------
0.1.3 | 2019-06-28 | Updated vignette
0.1.2 | 2019-05-15 | Changed from se:s to margins-of-error
0.1.1 | 2019-03-15 | makes, adds, removes, checks and triplot in place
0.1.0 | | 1st rough version.


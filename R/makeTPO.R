#' Prepare TriPlotObject
#'
#' @param scores Scores have observations in rows and components (or factors) in columns
#' @param loadings Loadings have variables in rows and components (or factors) in columns
#' @param compLimit Option limit to the number of component
#'
#' @return A TriPlotObject
#' @export
#'
#' @examples
#' See example under triPlot()
makeTPO <- function(scores,loadings,compLimit) {
  cat('\nMaking TriPlotObject (TPO)')
  cat('\n--------------------------')
  cat('\nPlease ensure that:')
  cat('\n  -Scores have observations in rows and components (or factors) in columns')
  cat('\n  -Loadings have variables in rows and components (or factors) in columns')
  cat('\n  -Scores and loadings have the same number of components\n')
  if (ncol(scores) != ncol(loadings)) stop("Not same number of components in scores and loadings.")
  nComp <- ncol(scores)
  if (nComp == 1) stop("1 component only not implemented.")
  if (missing(compLimit)) compLimit <- nComp
  if(compLimit > nComp) stop("compLimit cannot be larger than the number of components.")
  if(compLimit < nComp) cat('\nScores and loadings truncated from',nComp,'to',compLimit,'components.')
  scores <- scores[,1:compLimit]
  loadings <- loadings[,1:compLimit]
  nComp <- compLimit
  nObs <- nrow(scores)
  nVar <- nrow(loadings)
  cat('\n\nScore matrix has',nObs,'observations and',nComp,'components.')
  cat('\nLoading matrix has',nVar,'variables and',nComp,'components.')
  TPObject <- list()
  TPObject$scores <- scores
  TPObject$loadings <- loadings
  TPObject$nObs <- nObs
  TPObject$nVar <- nVar
  TPObject$nComp <- nComp
  TPObject$nCorr <- 0 # Initialize number of associated correlations
  TPObject$nRisk <- 0 # Initialize number of associated risks
  cat('\n\nTPO has',TPObject$nCorr,'attached correlations.')
  cat('\nTPO has',TPObject$nRisk,'attached risks.')
  return(TPObject)
}

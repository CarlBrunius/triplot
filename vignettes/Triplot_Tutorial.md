Package 'triplot'
=================

A novel tool for multivariate risk modelling and visualization that combines e.g. exposures, metabolome and disease risk.
-------------------------------------------------------------------------------------------------------------------------

**Maintainer**:<carl.brunius@chalmers.se>  
**URL**: <https://gitlab.com/CarlBrunius/triplot>  
**Date**: 2019-06-29

What is a triplot?
------------------

The triplot is the novel tool to facilitate visualization and
interpretation of multivariate risk modeling, which enables a global,
combined overview of information representing the metabolome (or other
types of multivariate data), exposure or environmental factors of
interest and associated health outcomes (i.e. disease outcomes or
intermediate risk factors). It integrates three levels of information:
1)loadings of latent variable (LV) modeling performed on a
high-dimensional dataset, generated from e.g. metabolomics, proteomics
or other omics;2)correlations between LV observation scores and single
or multiple exposures; 3) associations between the LV observation scores
and disease risk or intermediate risk factors.

Installation
------------

`devtools::install_gitlab("CarlBrunius/triplot", build_vignettes=T)`

Data
----

Prior to beginning the stepwise process of creating a triplot, make sure
that your data is in the correct format. Samples need to be in rows and
your metabolites or covariates need to be in columns.The triplot package
contains two synthetic datasets, i.e. 'HealthyNordicDiet.rda' and
'CAMP.rda'.

### HealthyNordicDiet

Description: HealthyNordicDiet was simulated from the authentic data
used in a case-control study nested within the Swedish prospective
Västerbotten Intervention Programme cohort. The original study was set
up to explore plasma metabolites that could objectively reflect healthy
Nordic dietary patterns in a matched case-control study and to assess
associations between such patterns and later development of type 2
diabetes. The simulated data contains three data frames: Clinical
measurements (ClinData, 11 variables), plasma metabolites predictive of
BMI (MetaboliteData, 20 variables), and dietary intake as measured by
food frequency questionnaires (FoodData, 11 variables). The data frames
are row-wise matched by observation and consist of 300 synthetic
observations. The data frames are row-wise matched by observation and
consist of 1000 observations that correspond to 500 case-control pairs
matched by gender and age.

Reference: Shi L, Brunius C, Johansson I, Bergdahl IA, Lindahl B,
Hanhineva K, et al. Plasma metabolites associated with healthy Nordic
dietary indexes and risk of type 2 diabetes—a nested case-control study
in a Swedish population. Am J Clin Nutr. 2018 Sep 1;108(3):564–75.

    library(triplot)
    data('HealthyNordicDiet')
    head(HealthyNordicDiet$BaselineData)

    ##      ID Group PairedInfo   Energy Gender      Age      BMI Smoking
    ## 1 47770     1          1 1433.718      1 58.08651 24.38661       3
    ## 2 80700     1          2 1505.547      1 57.33256 28.85224       3
    ## 3 70139     1          3 2131.331      1 52.24930 42.72100       3
    ## 4 13281     1          4 3621.582      1 46.53857 20.25507       2
    ## 5 47983     1          5 1293.078      2 43.14102 36.20921       3
    ## 6 98207     1          6 1207.406      2 52.71152 38.64588       2
    ##   PhysicalActivityIndex Education FastingGlucose
    ## 1                     3         2       8.503654
    ## 2                     1         2       7.390634
    ## 3                     1         3       7.108849
    ## 4                     4         3       6.160850
    ## 5                     2         4       6.674588
    ## 6                     3         4       7.379492

    head(HealthyNordicDiet$MetaboliteData)

    ##   Pipecolic.acid.betaine lysoPC.18.0. HP143.1179.1.55 lysoPE.22.6.
    ## 1               18342.29     71549.19        29666.72     12179.71
    ## 2               88477.37     58958.72        47627.01     13099.58
    ## 3               55575.74     29410.53        42561.10     10864.96
    ## 4               92773.49     41093.56        27102.06     39561.74
    ## 5               68512.86     84869.75        23207.94     14099.81
    ## 6               54124.71     90556.92        22712.35     23101.75
    ##   HN151.0065.4.97 RP225.1482.9.15 X3.4.5.Trimethoxycinnamic.acid       DHA
    ## 1        12842.22        4107.653                       30269.64  64781.88
    ## 2        23176.16       10250.291                       56978.78  99966.69
    ## 3        11369.12       14026.984                       63389.95 165548.82
    ## 4        18587.48       25432.107                      130459.35 132320.06
    ## 5        12903.45       23326.865                      124271.15 216523.10
    ## 6        44581.85       20246.212                       36932.40 114348.47
    ##   RP383.1671.10.53 gama.tocopherol RP431.3516.11.96 RP490.3516.8.99
    ## 1         28093.33       160741.50        12088.471        4540.939
    ## 2         37244.53       102536.19         9924.639       10171.194
    ## 3         65211.61        97916.69        10534.302       14811.063
    ## 4         41114.37        67615.94         9052.555       19301.125
    ## 5         70067.08       150042.60        13544.777       32545.801
    ## 6         42096.43        88017.83        10985.543       22334.734
    ##   lysoPC.22.6. Steroid.glucuronide RP684.5545.13.41 RP197.0926.3.17
    ## 1     825635.6            6667.399         8961.235        62194.48
    ## 2     833990.8           16189.730        37155.387        98060.22
    ## 3     967444.3            5033.863       109101.265        95687.15
    ## 4    1667615.0           69305.211        53771.070        52380.28
    ## 5    2187650.4            5114.792        75018.011        54137.15
    ## 6    1645681.4            7179.224        73401.890       114575.29
    ##   RP303.6468.9.98 RP816.5684.12.52 RP827.5356.11.88 RP826.5442.12.17
    ## 1        5351.424         23443.72         6790.108         8197.107
    ## 2        6526.964         50782.78        12794.064         8862.354
    ## 3       10364.784         45427.57        10431.554        11023.111
    ## 4       20072.759         49184.49        13961.937        21800.767
    ## 5       24990.161         60603.80        13881.765        13112.118
    ## 6       18114.168         40444.85        17613.730        15405.073
    ##   RN201.1498.8.23 RN203.0022.3.00      EPA PC.18.2.15.0. RN814.5610.12.14
    ## 1       16641.944        82055.36 23303.37      43102.75         9794.697
    ## 2        9729.933        18678.56 43557.31      39505.36         8296.960
    ## 3        6262.646        11134.27 70923.78      34343.56         7443.442
    ## 4       10426.436        41149.83 47606.81      23538.95         7322.638
    ## 5       16523.034        20490.91 73128.56      27835.33         7576.900
    ## 6        5740.037        21147.88 45885.57      42567.48         8893.629
    ##   RN153.0190.3.5 RN427.1641.10.59 RN457.1742.10.50 RN830.5845.12.46
    ## 1       8612.662         42034.54         10411.22         68066.59
    ## 2      13215.957         68985.67         16461.18        101176.01
    ## 3      10909.142        106284.72         18250.67         72630.75
    ## 4      13530.176         52556.98         11123.47         65861.99
    ## 5      23881.265         87433.29         14421.50         92643.25
    ## 6      31055.556         46264.17         14862.53         63868.43
    ##   RN446.1541.10.59 RN472.1596.10.53
    ## 1        14062.768          7252.72
    ## 2        16357.938         17099.86
    ## 3        12966.410         22764.88
    ## 4        17129.627         10481.75
    ## 5         9686.812         12457.42
    ## 6        13900.043         14547.13

    head(HealthyNordicDiet$FoodData)

    ##   HFI BSDS Wholegains    Sausage     Pizza Refined.bread   Fruits   Liquor
    ## 1   2   15   34.42774  0.4300682  8.011540      6.834202  0.00000 4.154152
    ## 2   4   15  116.39132  3.9724203 15.233692     13.023474 30.76790 8.729807
    ## 3   3   14   69.33574 16.6814430 16.640347     34.013064 77.94122 7.021280
    ## 4   1   10   58.06644  1.8046965  0.000000     23.667692  0.00000 9.506960
    ## 5   3   12  105.33583 12.3931773  4.495803      0.000000 79.74094 0.000000
    ## 6   5   20  122.74991  0.0000000  5.503491      0.000000 82.37470 1.789683
    ##        Wine Hamburger   Poultry       Fish Margarine   Cabbage     Carrot
    ## 1  0.000000  5.489759  6.183311  4.7217481 30.326262  0.000000  4.1216507
    ## 2  5.871873  7.512967  4.745613  8.5687444 29.812052  4.580903 22.4218334
    ## 3  0.000000 11.940310 13.876134 11.4189183 12.882245  0.000000  0.1491713
    ## 4  9.053440  0.000000  0.000000  0.9459515 24.473480  5.704976  2.6829291
    ## 5  7.078398  5.293979 13.590676 19.7295012 14.077744 21.513085 38.7222660
    ## 6 23.351060  0.000000  9.398423 24.0012817  9.168588 50.978922 74.9171717
    ##   low_fat_Dairy   Vegetables
    ## 1     33.050869   3.94867218
    ## 2    177.456402   5.77728024
    ## 3      6.777674   0.04487323
    ## 4     33.811401  20.77684095
    ## 5    126.931232  86.86364816
    ## 6     41.365576 204.61225350

### CAMP

Description: This synthetic dataset was simulated from the authentic
data used in a cross-sectional study of carbohydrate alternatives and
metabolic phenotypes (Liu et al. 2018). The simulated data contains
three data frames: Clinical measurements (ClinData, 11 variables),
plasma metabolites predictive of BMI (MetaboliteData, 20 variables), and
dietary intake as measured by food frequency questionnaires (FoodData,
11 variables). The data frames are row-wise matched by observation and
consist of 300 synthetic observations.

Reference: Xin Liu, Xia Liao, Wei Gan,Xinyun Ding, Bei Gao, Hao Wang,et
al.Inverse Relationship Between Coarse Food Grain Intake and Blood
Pressure Among Young Chinese Adults.Am J Hypertens.2018 December
8;<doi:10.1093/ajh/hpy187>

    data("CAMP")
    head(CAMP$ClinData)

    ##         AGE SEX      BMI triglycerides total cholesterol      HDL      LDL
    ## 1  16.28708   2 16.16602     0.8933667          4.347089 1.791262 2.143551
    ## 4  17.97382   2 18.19197     0.3204012          4.268551 1.804870 2.125259
    ## 18 23.41032   2 18.29741     0.7725975          3.683595 1.617793 1.780787
    ## 20 18.49269   2 19.45542     0.5201224          3.997766 1.172271 2.154114
    ## 22 25.86997   1 24.62681     0.8820070          3.212186 1.087429 1.848970
    ## 24 22.21763   2 20.87715     1.2011581          3.419748 1.557848 1.330810
    ##          GGT       ALT      AST creatinine Urea nitrogen Uric acid
    ## 1  12.041484  0.000000 21.87971   37.63294      3.435130  207.1574
    ## 4   9.619240  0.000000 25.08010   48.44006      2.569458  308.9231
    ## 18  9.407516  5.531870 18.97477   37.93843      4.388534  191.8850
    ## 20  7.090844  0.000000 21.42832   28.11382      3.160832  116.0145
    ## 22 28.709216 22.526391 29.34799   95.37657      5.912988  517.6168
    ## 24 15.179550  8.119861 18.57435   31.22524      1.936699  288.0659
    ##    Fasting glucose
    ## 1         4.892493
    ## 4         4.368838
    ## 18        5.453321
    ## 20        4.503950
    ## 22        5.485203
    ## 24        4.511535

    head(CAMP$MetaboliteData)

    ##    PC(P-20:0/22:6)
    ## 1         457938.2
    ## 4         423054.7
    ## 18        453786.2
    ## 20        376414.6
    ## 22        473822.0
    ## 24        259749.2
    ##    1-(3,4-Dihydroxyphenyl)-7-(4-hydroxy-3-methoxyphenyl)-1,6-heptadiene-3,5-dione
    ## 1                                                                        19292.01
    ## 4                                                                        17643.04
    ## 18                                                                       21511.55
    ## 20                                                                       12754.20
    ## 22                                                                       32234.00
    ## 24                                                                       19428.31
    ##    Unknown_675.6485@9.35 Unknown_1066.9034@10.16  PC(42:8)
    ## 1              102072.00                402182.0  704188.2
    ## 4              160362.96                361846.6 1129733.3
    ## 18             133445.58                381390.8 1220685.2
    ## 20             142203.43                363760.6 1060092.5
    ## 22             148818.02                297205.2 1261930.0
    ## 24              90940.27                300639.1 1066233.0
    ##    Unknown_931.7610@9.68 TG(52:0) CE(22:4)
    ## 1               117457.6 564710.9 46254.08
    ## 4                69173.3 663516.6 72799.08
    ## 18              211698.4 540359.7 29712.47
    ## 20              195551.5 655641.0 51928.20
    ## 22              321742.3 582476.7 11896.96
    ## 24              119031.9 445714.7 39492.30
    ##    Neutral glycosphingolipids1023.67 TG(58:10)
    ## 1                            1787342   4316310
    ## 4                            1858029   2703090
    ## 18                           1541564   3541104
    ## 20                           1705545   6227239
    ## 22                           1093728   8989640
    ## 24                           1531220   4201242
    ##    Neutral glycosphingolipids971.72 Unknown_914.7433@10.06 Cucurbic acid
    ## 1                          248834.5               469488.7      47415.65
    ## 4                          275971.6               342199.8      30695.74
    ## 18                         233943.7               434442.0      43256.65
    ## 20                         261203.5               414233.0      56980.00
    ## 22                         229388.0               525947.7      60880.96
    ## 24                         225221.5               407928.8      47900.22
    ##    DG(16:0/18:2) Indoleacrylic acid PC(O-20:0/22:6) Unknown_948.6589@8.51
    ## 1        1380138            6919071         8976781              527937.4
    ## 4        1015281            6376491         8012297              553644.6
    ## 18       1011667            7145628         8413625              572184.6
    ## 20       1056193            5164533         8560461              587566.7
    ## 22       1495421            6625016         8853730              560229.0
    ## 24       1894044            6263186         3072553              495206.9
    ##    Unknown_858.5318@5.49 O-propanoyl-carnitine  DG(38:5)
    ## 1                1224092               76654.5  90585.55
    ## 4                1184479              139197.8  60093.19
    ## 18               1228620              160493.0 115643.19
    ## 20               1121603              180453.7 122655.55
    ## 22               1067100              133347.1 177522.91
    ## 24               1209791              106260.2 199484.00

    head(CAMP$FoodData)

    ##    Refined grains Coarse grains  Red meat   Poutry   Seafood      Egg
    ## 1         0.00000       0.00000   0.00000  0.00000  0.000000  0.00000
    ## 4       145.42678      56.60885   0.00000 31.88803  0.000000  0.00000
    ## 18        0.00000      10.50187   0.00000 23.06393  0.000000  0.00000
    ## 20       78.96945      91.52483   0.00000 47.39014  0.000000 36.83117
    ## 22      262.64724       0.00000 120.09277  0.00000  9.399545 80.35676
    ## 24      488.82762      39.26516  53.16905 34.82579 13.623226 71.84187
    ##    Animal organs Vegetables    Fruits  Potatos    Legumes
    ## 1       0.000000   0.000000   0.00000  0.00000   5.757965
    ## 4       0.000000 101.021215 117.88017 55.13336 111.964904
    ## 18      0.000000   8.178762  25.74058 29.57040  13.844579
    ## 20      1.359505  73.826782 199.54778 91.24126  14.019282
    ## 22     10.627115 146.349141 109.07899 47.33487   0.000000
    ## 24      0.000000   3.704392 192.05003 32.21214  39.591437

Examples
--------

In this tutorial, using the ‘triplot’ approach, we show a global
overview of intercorrelations between plasma metabolites related to the
healthy Nordic diet, dietary intake variables as well as type 2 diabetes
(T2D) risk esimated using conditional (Example 1) or unconditional
logistic regression (Example 2) and intercorrelations between
BMI-related plasma metabolites, dietary intakes and metabolic traits
(PCA- or PLS based triplot, Example 3 and 4, respectively )

### Example 1

The dimensionality reduction technique, i.e. PCA, is first applied on
the metabolomics data. The associations between PCA scores and risk of
T2D were investigated using conditional logistic regression.PC scores
are also correlated with Healthy Nordic indices and food items using
Pearson correlations.

-   **Step 1: Load packages for relevant analyses**

<!-- -->

    library(triplot)### generate triplot for the data
    library(survival)### perform risk modeling, i.e. logistic regression analysis

-   **Step 2:Perform principal components analysis on index-related
    metabolites **

<!-- -->

    pca <-prcomp(HealthyNordicDiet$MetaboliteData, scale. = T, center=T)

In a scree plot, we simply plot the eigenvalues for all components, and
then look to see where they drop off sharply. This may help to determine
the number of components to extract.
![](Triplot_Tutorial_files/figure-markdown_strict/unnamed-chunk-5-1.png)

Exact scores and loadings from PCA modelling.

    scores=pca$x## Extract scores
    loadings=pca$rotation## Extract loadings

Here, we consider the first 2 components in the analysis in the tutorial
but users could determine number of components based on eigenvalue and
the criteria of Very Simple Structure. Users can also use 'checkTPO'
fuction to determine components of interest.

-   **Step3:Generate Triplot object**

<!-- -->

    nComp=2
    tpo <- makeTPO(scores = scores, loadings = loadings, compLimit = nComp)

    ## 
    ## Making TriPlotObject (TPO)
    ## --------------------------
    ## Please ensure that:
    ##   -Scores have observations in rows and components (or factors) in columns
    ##   -Loadings have variables in rows and components (or factors) in columns
    ##   -Scores and loadings have the same number of components
    ## 
    ## Scores and loadings truncated from 31 to 2 components.
    ## 
    ## Score matrix has 1000 observations and 2 components.
    ## Loading matrix has 31 variables and 2 components.
    ## 
    ## TPO has 0 attached correlations.
    ## TPO has 0 attached risks.

-   **Step4:Make a correlation matrix and attach the correlation matrix
    to the Triplot object**

We generate a correlation matrix between the TPO scores saved in the
‘tpo’ object and the dietary variables available in the data, using a
pair-wise Pearson correlations. Moreover, a study-specific correlation
matrix obtained from e.g. partial correlation adjusted for other
covariates could be prepared and used.

The correlation method could be "pearson", "kendall", "spearman". If
missing value exists, na.method includes "all.obs", "complete.obs",
"pairwise.complete.obs","everything", "na.or.complete".

    corMat <- makeCorr(tpo,HealthyNordicDiet$FoodData)

    ## 
    ## Making correlation between TPO and corrData
    ## -------------------------------------------
    ## Please ensure that rows (observations) are matched between TPO scores and corrData
    ## 
    ## Correlation matrix has 17 variables and 2 components

    head(corMat)[1:5]

    ## [1]  0.2720480  0.3272797  0.2098564 -0.1820406 -0.1989522

    tpo <- addCorr(tpo,corMat)

    ## 
    ## Adding correlation to TPO
    ## -------------------------
    ## Please ensure same number of components in TPO and correlation matrix
    ## 
    ## TPO has 17 attached correlations.
    ## TPO has 0 attached risks.

-   **Step5:Conduct the risk modelling. ** Here we calculate odds ratios
    (OR) of T2D for PC scores using conditional logistic regression,
    taking the matched case-control pair into consideration
    (strata=PairedInfo). A crude risk model with no adjustment is
    conducted and we add ORs from risk model to Triplot object.

<!-- -->

    risk <- crudeCLR(tpo,HealthyNordicDiet$BaselineData$Group,HealthyNordicDiet$BaselineData$PairedInfo)

    ## 
    ## Finished calculating risk for 2 components
    ##   column 1: mean values
    ##   column 2: margin-of-error
    ##   column 3: p-values

    risk

    ##           est        moe            p
    ## PC1 0.0651432 0.05178191 1.367482e-02
    ## PC2 0.7795398 0.13520073 1.300984e-29

    tpo <- addRisk(tpo,risk,'Risk') 

    ## 
    ## Adding risk to TPO
    ## ------------------
    ## Please add risks one by one with component in rows and estimates, se:s and (optionally) p-values in columns
    ## Please ensure same number of components in TPO and risk matrix
    ## 
    ## TPO has 17 attached correlations.
    ## TPO has 1 attached risks.

We could also conduct the risk model adjustment for covariates.
Covarites are user-defined. Here we conduct model adjusted for
BMI,smoking status, education, physical activity, and daily energy
intake (kcal per day).

      riskadj <- matrix(nrow = tpo$nComp, ncol = 3)
      colnames(riskadj) <- c("est", "se", "p")
      rownames(riskadj) <- colnames(tpo$scores)
      for (i in 1:tpo$nComp) {
        clr <- summary(clogit(Group ~ scores[, i]+BMI+as.factor(Smoking) +Energy+as.factor(PhysicalActivityIndex)+as.factor(Education)
                              +strata(PairedInfo),data=HealthyNordicDiet$BaselineData))
        riskadj[i, ] <- clr$coefficients[1,c(1, 3, 5)]
      }
    tpo <- addRisk(tpo,riskadj,'Riskadj')

    ## 
    ## Adding risk to TPO
    ## ------------------
    ## Please add risks one by one with component in rows and estimates, se:s and (optionally) p-values in columns
    ## Please ensure same number of components in TPO and risk matrix
    ## 
    ## TPO has 17 attached correlations.
    ## TPO has 2 attached risks.

    riskadj

    ##            est         se            p
    ## PC1 0.05408739 0.04204904 1.983408e-01
    ## PC2 0.53567193 0.08981087 2.454679e-09

We could generate a heatmap showing correlations between PCs, dietary
variables and ORs, to guide us in selecting the most interesting
componentsm using `checkTPO(tpo)`.

-   **Step6: Generate Triplot **

<!-- -->

    triPlot(tpo, comps = c(1,2), loadLabels = T,loadArrowLength = 0.1, plotLoads = TRUE,riskName='ORs of T2D', riskAntilog =TRUE )

![](Triplot_Tutorial_files/figure-markdown_strict/unnamed-chunk-12-1.png)
-------------------------------------------------------------------------

### Example 2

The unconditional logistic regression analysis is widely applicable to
epidemiologic studies for quantifying an association between a study
factor (i.e., an exposure variable) and a health outcome (i.e., disease
status). This could be achieved using glm() function. ORs of T2D
estimated from different methods could be presented in one triplot.
Instead, we could simply remove the logORs of T2D that are assessed
using conditional logistic regression from Triplot object and show logOR
from logistic regression only, by using function 'removeRisk'.

    removeRisk(tpo, remove = 'all')

    ## 
    ## Making TriPlotObject (TPO)
    ## --------------------------
    ## Please ensure that:
    ##   -Scores have observations in rows and components (or factors) in columns
    ##   -Loadings have variables in rows and components (or factors) in columns
    ##   -Scores and loadings have the same number of components
    ## 
    ## Scores and loadings truncated from 31 to 2 components.
    ## 
    ## Score matrix has 1000 observations and 2 components.
    ## Loading matrix has 31 variables and 2 components.
    ## 
    ## TPO has 0 attached correlations.
    ## TPO has 0 attached risks.

    ## 
    ## Making correlation between TPO and corrData
    ## -------------------------------------------
    ## Please ensure that rows (observations) are matched between TPO scores and corrData
    ## 
    ## Correlation matrix has 17 variables and 2 components

    ## 
    ## Adding correlation to TPO
    ## -------------------------
    ## Please ensure same number of components in TPO and correlation matrix
    ## 
    ## TPO has 17 attached correlations.
    ## TPO has 0 attached risks.

    ##           est         se            p
    ## PC1 0.0261267 0.03101143 3.995158e-01
    ## PC2 0.4796066 0.06462794 1.162034e-13

### Example 3

In this example, associations between PC components of BMI-related
plasma metabolites and other metabolic traits are investigated using
general linear model adjuted for age and gender. Correlations between PC
components and dietary variable are assessed using partial Pearson
correlations adjusted by age and gender.

    ## 
    ## Making TriPlotObject (TPO)
    ## --------------------------
    ## Please ensure that:
    ##   -Scores have observations in rows and components (or factors) in columns
    ##   -Loadings have variables in rows and components (or factors) in columns
    ##   -Scores and loadings have the same number of components
    ## 
    ## Scores and loadings truncated from 20 to 2 components.
    ## 
    ## Score matrix has 300 observations and 2 components.
    ## Loading matrix has 20 variables and 2 components.
    ## 
    ## TPO has 0 attached correlations.
    ## TPO has 0 attached risks.

Paritial correlations between PC scores and dietary variables.

    ##                ComponentPC1 ComponentPC2
    ## Refined grains   0.25892110  -0.01860154
    ## Coarse grains    0.05452461  -0.12435694
    ## Red meat         0.10822609  -0.08035956
    ## Poutry           0.11394038  -0.02065401
    ## Seafood         -0.03817776  -0.13012900

We consider the first 2 components and their association with other
clinical measurements are computed. Beta coefficients between each of
PCA scores and BMI are calculated and presented in triplot.

    nComp=2
    coefficients <- matrix(nrow = tpo$nComp, ncol = 3)
    colnames(coefficients) <- c("coefficients", "se", "p")
    for (i in 1:tpo$nComp) {
      glmod<-summary(glm(CAMP$ClinData$BMI~scores[,i]+as.factor(SEX)+AGE,data=CAMP$ClinData,family=gaussian))
      coefficients[i, ] <- glmod$coefficients[2,c(1, 2, 4)]
    }
    tpo <- addRisk(tpo,coefficients,'BMI')

    ## 
    ## Adding risk to TPO
    ## ------------------
    ## Please add risks one by one with component in rows and estimates, se:s and (optionally) p-values in columns
    ## Please ensure same number of components in TPO and risk matrix
    ## 
    ## TPO has 0 attached correlations.
    ## TPO has 1 attached risks.

The trplot that shows intercorrelations of metabolites, BMI, and dietary
variables is generated.

    tpo <- addCorr(tpo,corr.mat)

    ## 
    ## Adding correlation to TPO
    ## -------------------------
    ## Please ensure same number of components in TPO and correlation matrix
    ## 
    ## TPO has 11 attached correlations.
    ## TPO has 1 attached risks.

    triPlot(tpo, comps = c(1,2), loadCut = 0, loadLabels = T,plotLoads =TRUE,riskName=' PCA derived beta coefficients')

![](Triplot_Tutorial_files/figure-markdown_strict/unnamed-chunk-20-1.png)
Moreover, beta coefficients between PCA scores and other metabolic
traits can be computed and simply added to the tpo object. Here is an
example that we add the association with gamma-glutamyltransferase (GGT)
to the tiplot.

    coefficients <- matrix(nrow = tpo$nComp, ncol = 3)
    colnames(coefficients) <- c("coefficients", "se", "p")
    for (i in 1:tpo$nComp) {
      glmod<-summary(glm(CAMP$ClinData$GGT~scores[,i]+as.factor(SEX)+AGE,data=CAMP$ClinData,family=gaussian))
      coefficients[i, ] <- glmod$coefficients[2,c(1, 2, 4)]
    }
    tpo <- addRisk(tpo,coefficients,'GGT')

    ## 
    ## Adding risk to TPO
    ## ------------------
    ## Please add risks one by one with component in rows and estimates, se:s and (optionally) p-values in columns
    ## Please ensure same number of components in TPO and risk matrix
    ## 
    ## TPO has 11 attached correlations.
    ## TPO has 2 attached risks.

Correlation between PCs and dietary variables are added to tpo object.

    ## 
    ## Adding correlation to TPO
    ## -------------------------
    ## Please ensure same number of components in TPO and correlation matrix
    ## 
    ## TPO has 22 attached correlations.
    ## TPO has 2 attached risks.

![](Triplot_Tutorial_files/figure-markdown_strict/unnamed-chunk-22-1.png)
\#\#\# Example 4

In this example, supervised partial least squares (PLS) is performed on
plamsa metabolites predicting BMI using R packge ’mixOmics’.

For supervised LV modelling, number of components could be determined by
using repeated double cross validation (R package rdCV). This is a
package for rdCV of PLS and random forests for regression,
classification and multilevel type problems. Install rdCV package by:
devtools::install\_git(“<https://gitlab.com/CarlBrunius/rdCV.git>”).

    library(rdCV)
    library(doParallel)
    cl=makeCluster(2)
    registerDoParallel(cl)
    D=scale(log(CAMP$MetaboliteData),center = T,scale=T)
    rdCVMod=rdCV(X=D,Y=CAMP$ClinData$BMI,nRep=100,method='PLS')

We could also use the function perf (R package ‘mixOmics’) to evaluate a
PLS model and decide on the optimal number of components to choose using
K-fold cross-validation repeated 10 times. Detailed description is given
at <http://mixomics.org/methods/pls/>.

Here, we show associations between the first 2 components derived from
PLS modeling of BMI related metabolites, dietary intake and metabolic
traits.

    library(mixOmics)
    nComp=2
    PLSDATA=scale(CAMP$MetaboliteData,center = T,scale=T)
    pls=pls(PLSDATA,CAMP$ClinData$BMI,ncomp = nComp)
    scores=pls$variates$X
    loadings=pls$loadings$X
    tpo <- makeTPO(scores = scores, loadings = loadings, compLimit = nComp)

    ## 
    ## Making TriPlotObject (TPO)
    ## --------------------------
    ## Please ensure that:
    ##   -Scores have observations in rows and components (or factors) in columns
    ##   -Loadings have variables in rows and components (or factors) in columns
    ##   -Scores and loadings have the same number of components
    ## 
    ## 
    ## Score matrix has 300 observations and 2 components.
    ## Loading matrix has 20 variables and 2 components.
    ## 
    ## TPO has 0 attached correlations.
    ## TPO has 0 attached risks.

Similary, partial correlations between PLS components and dietary
varaibles are caculated and added to tpo object.

    ##                Componentcomp 1 Componentcomp 2
    ## Refined grains      0.24700630     -0.12266803
    ## Coarse grains       0.04361293     -0.09656196
    ## Red meat            0.10521761     -0.05047256
    ## Poutry              0.11884043      0.01979400
    ## Seafood            -0.04298003     -0.04915299

Associations of metabolites predicting BMI with BMI and GGT are
calculated and are added to tpo object for visulization.

    ## 
    ## Adding correlation to TPO
    ## -------------------------
    ## Please ensure same number of components in TPO and correlation matrix
    ## 
    ## TPO has 11 attached correlations.
    ## TPO has 0 attached risks.

    ## 
    ## Adding risk to TPO
    ## ------------------
    ## Please add risks one by one with component in rows and estimates, se:s and (optionally) p-values in columns
    ## Please ensure same number of components in TPO and risk matrix
    ## 
    ## TPO has 11 attached correlations.
    ## TPO has 1 attached risks.

    ## 
    ## Adding risk to TPO
    ## ------------------
    ## Please add risks one by one with component in rows and estimates, se:s and (optionally) p-values in columns
    ## Please ensure same number of components in TPO and risk matrix
    ## 
    ## TPO has 11 attached correlations.
    ## TPO has 2 attached risks.

![](Triplot_Tutorial_files/figure-markdown_strict/unnamed-chunk-26-1.png)
